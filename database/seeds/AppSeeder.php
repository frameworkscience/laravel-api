<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Team;
use App\Player;
class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Antonio',
            'email' => 'antonion@fwscience.com',
            'password' => bcrypt('secret'),
        ]);

        $teams = factory(\App\Team::class, 5)->create();
        $teams->each(function ($e_team){
            $team_players = factory(\App\Player::class,10)->make();
            $e_team->myPlayers()->saveMany($team_players);
        });
    }
}
