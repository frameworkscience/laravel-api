<?php

namespace App\Http\Controllers\API;

// dependencies
use App\Http\Controllers\Controller;
use App\Http\Requests\PlayerValidateRequest;

// Models
use App\Player;

class ApiPlayersController extends Controller
{
    /**
     * Gel All Teams Method
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayers(){
        try{
            $players = Player::orderBy('id', 'DESC')->get();
            return response()->json(['data' => $players], 200);
        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**
     *  Get Single Team
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlayer($id)
    {
        try{
            $players = Player::findOrFail($id);
            return response()->json(['data' => $players ], 200);
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**
     *  Create new Player
     * @param PlayerValidateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPlayer(PlayerValidateRequest $request)
    {
        try{
            $data_fields = $request->all();
            $player = Player::create($data_fields);

            return response()->json($player, 201);
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**
     *  Update Existing Player
     * @param PlayerValidateRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePlayer(PlayerValidateRequest $request, $id)
    {
        try{
            $players = Player::findOrFail($id);
            $rules = [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'team_id' => 'required|integer',
            ];
            $this->validate($request, $rules);

            if($request->has('first_name') || $request->has('last_name') | $request->has('team_id'))
            {
                $players->first_name = $request->first_name;
                $players->last_name = $request->last_name;
                $players->team_id = $request->team_id;
            }

            if(!$players->isDirty()) {
                return response()->json(['error' => 'You Need to Assign different value before update', 'code' => 422 ], 422);
            }

            $players->save();


            return response()->json(['data' => $players], 200);

        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**  Delete Team
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePlayer($id)
    {
        try{
            $players = Player::findOrFail($id);
            $players->delete();
            return response()->json(['data' => $players], 200);

        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }
}
