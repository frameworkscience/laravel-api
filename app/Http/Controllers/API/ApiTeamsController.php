<?php

namespace App\Http\Controllers\API;

// Dependencies
use App\Http\Requests\TeamValidationRequest;
use App\Http\Controllers\Controller;

// Models
use App\Team;

class ApiTeamsController extends Controller
{

    /**
     * Gel All Teams Method
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeams(){
        try{
            $teams = Team::orderBy('id', 'DESC')->get();
            return response()->json(['data' => $teams], 200);
        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**
     *  Get Single Team
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeam($id)
    {
        try{
            $team = Team::findOrFail($id);
            return response()->json(['data' => $team], 200);
        }catch (\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**
     *  Create new Team
     * @param TeamValidationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTeam(TeamValidationRequest $request)
    {
       try{
           $data_fields = $request->all();
           $team = Team::create($data_fields);
           return response()->json($team, 201);
       }catch (\Exception $e){
           return response()->json($e, 403);
       }
    }

    /**
     *  Update Existing Team
     * @param TeamValidationRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTeam(TeamValidationRequest $request, $id)
    {
        try{
            $teams = Team::findOrFail($id);
            $rules = [
                'name' => 'required'
            ];
            $this->validate($request, $rules);

            if($request->has('name'))
            {
                $teams->name = $request->name;
            }

            // Validate if input value is different to update
            if(!$teams->isDirty()) {
                return response()->json(['error' => 'You Need to Assign different value before update', 'code' => 422 ], 422);
            }

            $teams->save();
            return response()->json($teams, 200);

        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }

    /**  Delete Team
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTeam($id)
    {
        try{
            $teams = Team::findOrFail($id);
            $teams->delete();
            return response()->json(['data' => $teams], 200);

        }catch(\Exception $e){
            return response()->json($e, 403);
        }
    }
}
