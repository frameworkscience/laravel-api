<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/teams', 'API\ApiTeamsController@getTeams');
Route::get('/team/{id}', 'API\ApiTeamsController@getTeam');
Route::post('/create-team', 'API\ApiTeamsController@createTeam');
Route::put('/update-team/{id}', 'API\ApiTeamsController@updateTeam');
Route::delete('/delete-team/{id}', 'API\ApiTeamsController@deleteTeam');



Route::get('/players', 'API\ApiPlayersController@getPlayers');
Route::get('/players/{id}', 'API\ApiPlayersController@getPlayer');
Route::post('/create-player', 'API\ApiPlayersController@createPlayer');
Route::put('/update-player/{id}', 'API\ApiPlayersController@updatePlayer');
Route::delete('/delete-player/{id}', 'API\ApiPlayersController@deletePlayer');

